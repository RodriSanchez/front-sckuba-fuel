import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import {
  copecCard,
  copecCardActions,
  copecCardContent,
  copecCardHeader,
  copecCardHeaderActions,
  copecCardHeaderSubTitle,
  copecCardHeaderTitle
} from './card.component';

const cardComponents = [
  copecCard,
  copecCardHeader,
  copecCardHeaderTitle,
  copecCardHeaderSubTitle,
  copecCardHeaderActions,
  copecCardContent,
  copecCardActions
];

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    ...cardComponents
  ],
  exports: [
    ...cardComponents
  ]
})
export class copecCardModule {
}
