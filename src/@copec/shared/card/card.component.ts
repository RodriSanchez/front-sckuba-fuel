import { ChangeDetectionStrategy, Component, Directive, Input, ViewEncapsulation } from '@angular/core';

// noinspection TsLint
@Component({
  selector: 'copec-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss'],
  host: { 'class': 'copec-card' },
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class copecCard {
}

// noinspection TsLint
@Component({
  selector: 'copec-card-header',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
  host: { 'class': 'copec-card-header' },
  template: `
    <div class="copec-card-header-heading-group">
      <ng-content select="copec-card-header-heading"></ng-content>
      <ng-content select="copec-card-header-subheading"></ng-content>
    </div>
    <ng-content></ng-content>
    <ng-content select="copec-card-header-actions"></ng-content>
  `
})
export class copecCardHeader {
}

// noinspection TsLint
@Component({
  selector: 'copec-card-content',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
  host: { 'class': 'copec-card-content' },
  template: `
    <ng-content></ng-content>`
})
export class copecCardContent {
}

// noinspection TsLint
@Directive({
  selector: 'copec-card-header-heading',
  host: { 'class': 'copec-card-header-heading' }
})
export class copecCardHeaderTitle {
}

// noinspection TsLint
@Directive({
  selector: 'copec-card-header-subheading',
  host: { 'class': 'copec-card-header-subheading' }
})
export class copecCardHeaderSubTitle {
}

// noinspection TsLint
@Directive({
  selector: 'copec-card-header-actions',
  host: { 'class': 'copec-card-header-actions' }
})
export class copecCardHeaderActions {
}

// noinspection TsLint
@Directive({
  selector: 'copec-card-actions',
  host: {
    'class': 'copec-card-actions',
    '[class.copec-card-actions-align-end]': 'align === "end"',
  }
})
export class copecCardActions {
  /** Position of the actions inside the card. */
  @Input() align: 'start' | 'end' = 'start';
}
