import { Directive } from '@angular/core';

@Directive({
  selector: '[copecTitle],copec-title',
  host: {
    class: 'copec-title'
  }
})
export class TitleDirective {

  constructor() { }

}
