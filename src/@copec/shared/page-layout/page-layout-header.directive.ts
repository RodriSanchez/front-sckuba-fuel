import { Directive } from '@angular/core';

@Directive({
  selector: '[copecPageLayoutHeader],copec-page-layout-header',
  host: {
    class: 'copec-page-layout-header'
  }
})
export class PageLayoutHeaderDirective {

  constructor() { }

}

