import { Directive } from '@angular/core';

@Directive({
  selector: '[copecPageLayoutContent],copec-page-layout-content',
  host: {
    class: 'copec-page-layout-content'
  }
})
export class PageLayoutContentDirective {

  constructor() { }

}
