import { Directive, HostBinding, Input } from '@angular/core';

@Directive({
  selector: '[copecPageLayout],copec-page-layout',
  host: {
    class: 'copec-page-layout'
  }
})
export class PageLayoutDirective {

  @Input() mode: 'card' | 'simple' = 'simple';

  constructor() { }

  @HostBinding('class.copec-page-layout-card')
  get isCard() {
    return this.mode === 'card';
  }

  @HostBinding('class.copec-page-layout-simple')
  get isSimple() {
    return this.mode === 'simple';
  }

}
