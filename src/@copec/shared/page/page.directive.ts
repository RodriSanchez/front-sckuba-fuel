import { Directive } from '@angular/core';

@Directive({
  selector: '[copecPage],copec-page',
  host: {
    class: 'copec-page'
  }
})
export class PageDirective {

  constructor() { }

}
