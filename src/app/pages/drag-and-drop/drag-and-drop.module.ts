import { DragDropModule } from '@angular/cdk/drag-drop';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { BreadcrumbsModule } from '../../../@copec/shared/breadcrumbs/breadcrumbs.module';
import { copecCardModule } from '../../../@copec/shared/card/card.module';
import { MaterialModule } from '../../../@copec/shared/material-components.module';
import { ScrollbarModule } from '../../../@copec/shared/scrollbar/scrollbar.module';
import { DragAndDropRoutingModule } from './drag-and-drop-routing.module';
import { DragAndDropComponent } from './drag-and-drop.component';
import { CopecSharedModule } from '../../../@copec/copec-shared.module';

@NgModule({
  imports: [
    CommonModule,
    DragAndDropRoutingModule,
    CopecSharedModule,
    MaterialModule,
    ReactiveFormsModule,
    ScrollbarModule,
    DragDropModule,
    BreadcrumbsModule,
    copecCardModule
  ],
  declarations: [DragAndDropComponent]
})
export class DragAndDropModule {
}
