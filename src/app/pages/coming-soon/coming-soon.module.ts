import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ComingSoonRoutingModule } from './coming-soon-routing.module';
import { ComingSoonComponent } from './coming-soon.component';
import { CopecSharedModule } from '../../../@copec/copec-shared.module';
import { copecCardModule } from '../../../@copec/shared/card/card.module';

@NgModule({
  imports: [
    CommonModule,
    ComingSoonRoutingModule,
    CopecSharedModule,
    copecCardModule
  ],
  declarations: [ComingSoonComponent]
})
export class ComingSoonModule {
}
