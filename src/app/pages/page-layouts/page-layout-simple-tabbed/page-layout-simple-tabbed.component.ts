import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'copec-page-layout-simple-tabbed',
  templateUrl: './page-layout-simple-tabbed.component.html',
  styleUrls: ['./page-layout-simple-tabbed.component.scss']
})
export class PageLayoutSimpleTabbedComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
