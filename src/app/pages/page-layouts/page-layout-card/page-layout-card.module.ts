import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PageLayoutCardRoutingModule } from './page-layout-card-routing.module';
import { PageLayoutCardComponent } from './page-layout-card.component';
import { CopecSharedModule } from '../../../../@copec/copec-shared.module';
import { PageLayoutDemoContentModule } from '../components/page-layout-content/page-layout-demo-content.module';
import { copecCardModule } from '../../../../@copec/shared/card/card.module';

@NgModule({
  declarations: [PageLayoutCardComponent],
  imports: [
    CommonModule,
    PageLayoutCardRoutingModule,
    CopecSharedModule,
    copecCardModule,
    PageLayoutDemoContentModule
  ]
})
export class PageLayoutCardModule {
}
