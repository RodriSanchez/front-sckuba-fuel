import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { BreadcrumbsModule } from '../../../@copec/shared/breadcrumbs/breadcrumbs.module';
import { copecCardModule } from '../../../@copec/shared/card/card.module';
import { MaterialModule } from '../../../@copec/shared/material-components.module';
import { IconsRoutingModule } from './icons-routing.module';
import { IconsComponent } from './icons.component';

@NgModule({
  imports: [
    CommonModule,
    IconsRoutingModule,
    ReactiveFormsModule,
    MaterialModule,
    BreadcrumbsModule,
    copecCardModule
  ],
  declarations: [IconsComponent]
})
export class IconsModule {
}
