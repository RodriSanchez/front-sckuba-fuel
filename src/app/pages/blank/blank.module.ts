import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BlankRoutingModule } from './blank-routing.module';
import { BlankComponent } from './blank.component';
import { CopecSharedModule } from '../../../@copec/copec-shared.module';

@NgModule({
  imports: [
    CommonModule,
    BlankRoutingModule,
    CopecSharedModule
  ],
  declarations: [BlankComponent]
})
export class BlankModule {
}
