import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { BreadcrumbsModule } from '../../../../@copec/shared/breadcrumbs/breadcrumbs.module';
import { copecCardModule } from '../../../../@copec/shared/card/card.module';
import { HighlightModule } from '../../../../@copec/shared/highlightjs/highlight.module';
import { MaterialModule } from '../../../../@copec/shared/material-components.module';
import { FormElementsRoutingModule } from './form-elements-routing.module';
import { FormElementsComponent } from './form-elements.component';
import { CopecSharedModule } from '../../../../@copec/copec-shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormElementsRoutingModule,
    MaterialModule,
    CopecSharedModule,
    ReactiveFormsModule,

    // Core
    HighlightModule,
    copecCardModule,
    BreadcrumbsModule
  ],
  declarations: [FormElementsComponent]
})
export class FormElementsModule {
}
