import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { copecCardModule } from '../../../../../@copec/shared/card/card.module';
import { ListModule } from '../../../../../@copec/shared/list/list.module';
import { LoadingOverlayModule } from '../../../../../@copec/shared/loading-overlay/loading-overlay.module';
import { MaterialModule } from '../../../../../@copec/shared/material-components.module';
import { RecentSalesWidgetTableComponent } from './recent-sales-widget-table/recent-sales-widget-table.component';
import { RecentSalesWidgetComponent } from './recent-sales-widget.component';
import { ChartsModule } from 'ng2-charts';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,

    // Core
    LoadingOverlayModule,
    copecCardModule,
    ListModule,
    ChartsModule
  ],
  declarations: [RecentSalesWidgetComponent, RecentSalesWidgetTableComponent],
  exports: [RecentSalesWidgetComponent]
})
export class RecentSalesWidgetModule {
}
