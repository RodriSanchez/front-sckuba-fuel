import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { copecCardModule } from '../../../../../@copec/shared/card/card.module';
import { LoadingOverlayModule } from '../../../../../@copec/shared/loading-overlay/loading-overlay.module';
import { MaterialModule } from '../../../../../@copec/shared/material-components.module';
import { ScrollbarModule } from '../../../../../@copec/shared/scrollbar/scrollbar.module';
import { AdvancedPieChartWidgetComponent } from './advanced-pie-chart-widget.component';
import { ChartsModule } from 'ng2-charts';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,

    // Core
    copecCardModule,
    LoadingOverlayModule,
    ScrollbarModule,
    ChartsModule
  ],
  declarations: [AdvancedPieChartWidgetComponent],
  exports: [AdvancedPieChartWidgetComponent]
})
export class AdvancedPieChartWidgetModule {
}
