import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { LoadingOverlayModule } from '../../../../../@copec/shared/loading-overlay/loading-overlay.module';
import { MaterialModule } from '../../../../../@copec/shared/material-components.module';
import { LineChartWidgetComponent } from './line-chart-widget.component';
import { copecCardModule } from '../../../../../@copec/shared/card/card.module';
import { ChartsModule } from 'ng2-charts';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,

    // Core
    LoadingOverlayModule,

    // Chart Widget Style
    copecCardModule,
    ChartsModule
  ],
  declarations: [LineChartWidgetComponent],
  exports: [LineChartWidgetComponent]
})
export class LineChartWidgetModule {
}
