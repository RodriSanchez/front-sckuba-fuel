import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { copecCardModule } from '../../../../../@copec/shared/card/card.module';
import { LoadingOverlayModule } from '../../../../../@copec/shared/loading-overlay/loading-overlay.module';
import { MaterialModule } from '../../../../../@copec/shared/material-components.module';
import { SalesSummaryWidgetComponent } from './sales-summary-widget.component';
import { ChartsModule } from 'ng2-charts';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,

    // Core
    LoadingOverlayModule,
    copecCardModule,
    ChartsModule
  ],
  declarations: [SalesSummaryWidgetComponent],
  exports: [SalesSummaryWidgetComponent]
})
export class SalesSummaryWidgetModule {
}
